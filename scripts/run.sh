#!/bin/bash
set -x

PROJECT_DIR="/home/dxble/workspace/faultlocalizationTools/jaguar/br.usp.each.saeg.jaguar.example"
JACOCO_JAR="/home/dxble/workspace/faultlocalizationTools/jaguar/br.usp.each.saeg.jaguar.plugin/lib/jacocoagent.jar"
LOG_LEVEL="TRACE" # ERROR / INFO / DEBUG / TRACE
JRELIFIX_JAR="/home/dxble/workspace/JRelifix/build/libs/JRelifix-1.0-SNAPSHOT-all.jar"
LOCALLIBS="/home/dxble/workspace/JRelifix/libs/br.usp.each.saeg.jaguar.core-1.0.0-jar-with-dependencies.jar:/home/dxble/workspace/JRelifix/libs/jacocoagent.jar"
#BR="/home/dxble/workspace/JRelifix/libs/br.usp.each.saeg.jaguar.core-1.0.0-jar-with-dependencies.jar"
#JDT="/home/dxble/.gradle/caches/modules-2/files-2.1/org.eclipse.jdt/org.eclipse.jdt.core/3.16.0/c0767741a4699ef4d2b657b820f8e5def839a53f/org.eclipse.jdt.core-3.16.0.jar"
# CONTROL-FLOW

java -javaagent:$JACOCO_JAR=output=tcpserver -cp $PROJECT_DIR/target/classes/:$PROJECT_DIR/target/test-classes/:$JRELIFIX_JAR:$JACOCO_JAR:$LOCALLIBS \
            driver.Main \
			--projectFolder $PROJECT_DIR \
			--sourceFolder $PROJECT_DIR/src/main/java \
			--testFolder $PROJECT_DIR/src/test/java \
			--sourceClassFolder "$PROJECT_DIR/target/classes/" \
			--testClassFolder "$PROJECT_DIR/target/test-classes/"\
			--topNFaultLoc 1
