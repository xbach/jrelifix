package utils;

/**
 * Created by dxble on 8/27/16.
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import driver.Main;
import params.OptParser;

/**
 * Created by xuanbach32bit on 8/26/15.
 */
public class FileFolderUtils {

    public static ArrayList<File> walk(String path, String to_find, ArrayList<File> found) {
        File root = new File(path);
        File[] list = root.listFiles();
        if (list == null) {
            return null;
        } else {
            File[] arr$ = list;
            int len$ = list.length;

            for(int i$ = 0; i$ < len$; ++i$) {
                File f = arr$[i$];
                if (f.isDirectory()) {
                    walk(f.getAbsolutePath(), to_find, found);
                } else {
                    String fname = f.getAbsoluteFile().toString();
                    if (fname.contains(to_find)) {
                        found.add(f);
                    }
                }
            }

            return found;
        }
    }

    public static void walkRecString(String path, String extension, ArrayList<String> found) {

        File root = new File( path );
        File[] list = root.listFiles();

        if (list == null) return;

        for ( File f : list ) {
            if ( f.isDirectory() ) {
                walkRecString( f.getAbsolutePath(), extension, found );
                //System.out.println( "Dir:" + f.getAbsoluteFile() );
            }
            else {
                //System.out.println( "File:" + f.getAbsoluteFile() );
                if(f.getName().endsWith(extension))
                    found.add(f.getAbsolutePath());

            }
        }
    }

    public static void walkRec(String path, String extension, ArrayList<File> found) {

        File root = new File( path );
        File[] list = root.listFiles();

        if (list == null) return;

        for ( File f : list ) {
            if ( f.isDirectory() ) {
                walkRec( f.getAbsolutePath(), extension, found );
                //System.out.println( "Dir:" + f.getAbsoluteFile() );
            }
            else {
                //System.out.println( "File:" + f.getAbsoluteFile() );
                if(f.getName().endsWith(extension))
                    found.add(f);

            }
        }
    }

    public static List<File> search4FilesContainName(File dir, String containName) {
        List<File> ans = new LinkedList();
        Queue<File> queue = new LinkedList();
        queue.add(dir);

        while(true) {
            while(!queue.isEmpty()) {
                File e = (File)queue.poll();
                if (e.isFile()) {
                    if (e.getName().contains(containName) && !e.getName().endsWith("~")) {
                        ans.add(e);
                    }
                } else {
                    File[] lst = e.listFiles();
                    File[] arr = lst;
                    int len = lst.length;

                    for(int i = 0; i< len; ++i) {
                        File f = arr[i];
                        queue.add(f);
                    }
                }
            }

            return ans;
        }
    }

    public static String relativePath(String base, String pathFull){
        Path pathAbsolute = Paths.get(pathFull);
        Path pathBase = Paths.get(base);
        Path pathRelative = pathBase.relativize(pathAbsolute);
        return pathRelative.toString();
    }

    public static String path2Package(String path){
        String normalize = path;
        if(path.charAt(0) == '/')
            normalize = path.substring(1, path.length());
        normalize =normalize.split("\\.")[0]; // Strip extentions such as .java
        normalize = normalize.split("\\$")[0];
        return normalize.replace(File.separator, ".");
    }

    public static void removeDir(String dir) throws IOException {
        File dirin = new File(dir);
        try{
            org.apache.commons.io.FileUtils.deleteDirectory(dirin);
        }catch(Exception ex){
            //Retry
            //FileFolderUtils.deleteDirectory(dirin);
            //logger.error("ex: "+ex.getMessage());
        }
        //dirin.mkdir();
    }

    public static boolean copyOriginalBin(String inDir, String baseIdenDir, String mutatorIdentifier) throws IOException {
        boolean copied = false;
        if(inDir != null){
            File original = new File(inDir);
            String tempDirFullPath = baseIdenDir + File.separator + mutatorIdentifier;
            File dest = new File(tempDirFullPath);
            dest.mkdirs();
            org.apache.commons.io.FileUtils.copyDirectory(original, dest);
            copied = true;
        }
        return copied;
    }


    /**
     * Set up a project for a given mutator identifier.
     * @param tempDirNumber
     * @throws IOException
     */
    public synchronized static String setupTempDirectories(String applicationBinDir,String testBinDir,String baseTempDir, String tempDirNumber) throws IOException {

        //cleanMutationResultDirectories();
        String fullTempDir = baseTempDir+ File.separator + tempDirNumber;
        removeDir(fullTempDir);
        //copyOriginalCode(currentMutatorIdentifier);
        try{
            boolean copied1 = copyOriginalBin(applicationBinDir,baseTempDir, tempDirNumber);// NEW
            // ADDED
            boolean copied2 = copyOriginalBin(testBinDir,baseTempDir, tempDirNumber);// NEW
        }catch(Exception e){
            e.printStackTrace();
        }																							// ADDED

        return fullTempDir;
        //copyData(currentMutatorIdentifier);

    }

    public static URL[] getURLforInstrumented() throws MalformedURLException {
        ArrayList<String> deps = new ArrayList<>(Arrays.asList(OptParser.parsedParams().classPath().split(":")));
        List<URL> classpath = new ArrayList<>();
        for(String dep: deps){
            classpath.add(new File(dep).toURI().toURL());
        }
        //bin
        URL urlBin = new File(getInstrumentedDirWithPrefix()).toURI().toURL();
        classpath.add(urlBin);

        URL[] cp = classpath.toArray(new URL[0]);
        return cp;
    }

    public static String getInstrumentedDirWithPrefix() {
        String fullPath = System.getProperty("ANGELIX_FOLDER") + File.separator + "instrumented";
        return fullPath;
    }

    public static URL[] redefineURL(File foutgen, URL[] originalURL) throws MalformedURLException {
        List<URL> urls = new ArrayList<URL>();
        urls.add(foutgen.toURL());// remember that we need to add the foutgen, the one we need to redefine at the beginning
        for (int i = 0; (originalURL != null) && i < originalURL.length; i++) {
            urls.add(originalURL[i]);
        }

        return urls.toArray(originalURL);
    }

    public static String fileNameAbsolutePath(String fileName){
        return OptParser.parsedParams().sourceFolder() + /*File.separator+*/ fileName.replace(".", File.separator) +".java";
    }

    public static void write2File(String fileName, String content) throws IOException {
        FileWriter writer = new FileWriter(fileName);
        BufferedWriter bw = new BufferedWriter(writer);
        bw.write(content);
        bw.close();
    }
}
