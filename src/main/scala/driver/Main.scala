package driver

import java.io.File

import localization._
import params.{OptParser}
import org.apache.log4j.Logger
import parser.javaparser.JavaParser

object Main {
  val logger = Logger.getLogger(this.getClass)

  def main(args: Array[String]): Unit = {
    OptParser.parseOpts(args)

    logger.info("Trying to set up fault localization! ")

    var locLib: LocalizationLibrary = null

    logger.info("Doing localization with %s, heuristic: %s".format(+OptParser.parsedParams().locLibName, OptParser.parsedParams().locHeuristic))
    if(OptParser.parsedParams().faultLines != null){ // Fault information is given manually
      locLib = new PredefinedFaults(OptParser.parsedParams().faultLines)
    }
    else if(OptParser.parsedParams().locLibName == LocalizationLibName.JAGUAR){ // Fault information needs to be discovered by fault localization tools
      try {
        import br.usp.each.saeg.jaguar.core.heuristic.Heuristic
        val locHeuristic = Class.forName("br.usp.each.saeg.jaguar.core.heuristic.%sHeuristic".format(OptParser.parsedParams().locHeuristic)).newInstance.asInstanceOf[Heuristic]
        val locConfig = new JaguarConfig(locHeuristic, new File(OptParser.parsedParams().projFolder), new File(OptParser.parsedParams().sourceClassFolder), new File(OptParser.parsedParams().testClassFolder), OptParser.parsedParams().isDataFlow)
        locLib = new JaguarLocalization(locConfig.asInstanceOf[JaguarConfig])
        locLib.runner()
      } catch {
        case e: Exception => {
          logger.error(e.getMessage)
          return
        }
      }
    }

    // Output ranked list of buggy statements in original form
    // Note: this is not yet very memory efficient because later we only need top N, not all in the rank list.
    // So we can potentially just make rankedList to be top N directly. This is an easy improvement, but let's see
    // if we need it or not later.
    logger.info(locLib.rankedList)

    logger.info("Done localization!")

    //TODO: To use DiffUtils to further filter rankedList as mentioned on the original paper, Section V.A

    // Warning: can be a lot of fault Files if the topNFaultLoc from rankedList is large enough. May need improvement later ...
    logger.info("Considering top %d fault locations".format(OptParser.parsedParams().topNFaultLoc))
    val topNFaults = locLib.rankedList.take(OptParser.parsedParams().topNFaultLoc)
    val faultFiles = topNFaults.collect{case i => i.getFileName()}.toSet

    logger.info("Now do parsing ...")
    JavaParser.batchParse(OptParser.parsedParams().sourceFolder, OptParser.parsedParams().classPath, faultFiles)

    logger.info("Now do transform each fault node to a Java node ...")
    // Note: to transform to JavaNode, we must do parsing first. See JavaParser above
    topNFaults.foreach(_.transformToJavaNode())
    // Output ranked list of buggy statements in AST node form
    topNFaults.foreach(f => logger.info(f.getJavaNode()))
    logger.info("Ended!")
  }
}
