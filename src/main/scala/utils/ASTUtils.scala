package utils

/**
  * Created by dxble on 8/30/16.
  */

import org.eclipse.jdt.core.dom._
import org.apache.log4j.Logger
import java.util.Set
import java.util.TreeSet

import org.eclipse.jface.text.{Document}
import localization.{FaultIdentifier, Identifier}
import org.eclipse.jdt.core.dom.rewrite.{ASTRewrite, ListRewrite}

import scala.collection.mutable
import scala.collection.mutable._

class FindASTNodeForIdentifier(cu: CompilationUnit, to_find: Identifier[Any]) extends ASTVisitor {
  var found: ASTNode = null

  override def preVisit2(node: ASTNode): Boolean = {
    if (to_find.sameLocation(node)) {
      //System.out.println("Found Node")
      //System.out.println("BL" + node.toString + " *** " + node.getClass + " LN:" + line + " Iden:" + to_find.getLine)
      found = node
      return false
    }
    return true
  }

  def getCompilationUnit: CompilationUnit = {
    return this.cu
  }
}

object ASTUtils {
  private val logger: Logger = Logger.getLogger(ASTUtils.getClass)

  // Replace node by replacement
  def replaceNode(rew: ASTRewrite, node: ASTNode, replacement: ASTNode): ASTRewrite = {
    var rep: ASTNode = null
    rep = ASTNode.copySubtree(node.getAST(), replacement)
    rew.replace(node, rep, null)
    return rew
  }

  def appendNode(rewriter: ASTRewrite, parent: ASTNode, child: ASTNode): ASTRewrite = {
    if (child == null) throw new Exception("This should never happen")
    val to_add: ASTNode = ASTNode.copySubtree(parent.getAST(), child)
    val bl: Block = parent.getParent.asInstanceOf[Block]
    val rewrite: ListRewrite = rewriter.getListRewrite(bl, Block.STATEMENTS_PROPERTY)
    rewrite.insertAfter(to_add, parent, null)
    return rewrite.getASTRewrite
  }

  def insertBeforeNode(rewriter: ASTRewrite, parent: ASTNode, child: ASTNode): ASTRewrite = {
    if (child == null) throw new Exception("This should never happen")
    val to_add: ASTNode = ASTNode.copySubtree(parent.getAST(), child)
    if(parent.getParent.isInstanceOf[Block]) {
      val bl: Block = parent.getParent.asInstanceOf[Block]
      val rewrite: ListRewrite = rewriter.getListRewrite(bl, Block.STATEMENTS_PROPERTY)
      rewrite.insertBefore(to_add, parent, null)
      return rewrite.getASTRewrite
    }else{
      val bl: Block = parent.getAST.newBlock()
      InstrumentUtils.addStatement2Block(bl, ASTNode.copySubtree(bl.getAST, child))
      InstrumentUtils.addStatement2Block(bl, ASTNode.copySubtree(bl.getAST, parent))

      return replaceNode(rewriter, parent, bl)
    }
  }

  def findNode(cu: CompilationUnit, to_find: Identifier[Any]): ASTNode = {
    //if(to_find.isInstanceOf[LineIden]){
    val find = new FindASTNodeForIdentifier(cu, to_find)
    if(cu == null) {
      println("CU NULL")
      return null //TODO: to check
    }
    cu.accept(find)
    return find.found
    //}
    //return null
  }

  def createFaultIdentifierNoClassName(node: ASTNode): FaultIdentifier = {
    val cu: CompilationUnit = node.getRoot.asInstanceOf[CompilationUnit]
    val nodeLength: Int = node.getLength

    val bl: Int = cu.getLineNumber(node.getStartPosition)
    val el: Int = cu.getLineNumber(node.getStartPosition + nodeLength)
    val bc: Int = cu.getColumnNumber(node.getStartPosition) + 1
    val ec: Int = cu.getColumnNumber(node.getStartPosition + nodeLength) + 1
    return FaultIdentifier(Array(bl.toString, bc.toString, el.toString, ec.toString), null)
  }

  def sameASTNode(node1: ASTNode, node2: ASTNode): Boolean ={
    return node1.toString.equals(node2.toString)
  }

  def compatibleTypes(t1: ITypeBinding, t2: ITypeBinding): Boolean = {
    if (t1 == null || t2 == null) return false
    if (t1.isSubTypeCompatible(t2) || t1.isCastCompatible(t2) || t1.isAssignmentCompatible(t2) || (t1.toString == t2.toString)) {
      return true
    }
    else return false
  }

  def strictlyCompatibleTypes(t1: ITypeBinding, t2: ITypeBinding): Boolean = {
    if (t1 == null && t2 == null) return true
    else if(t1 == null && t2 != null)
      return false
    else if(t1 !=null && t2 == null)
      return false
    else if (t1.toString.compareTo(t2.toString) == 0) {
      return true
    }
    else return false
  }

  def getStatementLineNo(node: ASTNode): Int = {
    val root: ASTNode = node.getRoot
    var lineno: Int = -1
    if (root.isInstanceOf[CompilationUnit]) {
      val cu: CompilationUnit = root.asInstanceOf[CompilationUnit]
      lineno = cu.getLineNumber(node.getStartPosition)
    }
    else {
      logger.error("Root is not CU?")
    }
    return lineno
  }

  def getStatementEndLineNo(node: ASTNode): Int = {
    val root: ASTNode = node.getRoot
    var lineno: Int = -1
    if (root.isInstanceOf[CompilationUnit]) {
      val cu: CompilationUnit = root.asInstanceOf[CompilationUnit]
      lineno = cu.getLineNumber(node.getStartPosition + node.getLength)
    }
    else {
      logger.error("Root is not CU?")
    }
    return lineno
  }

  def getNames(node: ASTNode): Set[String] = {
    val names: TreeSet[String] = new TreeSet[String]
    if (node != null) {
      val visitor: NameCollector = new NameCollector(names)
      node.accept(visitor)
    }
    return names
  }

  def getTypes(node: ASTNode): Set[String] = {
    val types: TreeSet[String] = new TreeSet[String]
    if (node != null) {
      val visitor: TypeCollector = new TypeCollector(types)
      node.accept(visitor)
    }
    return types
  }

  def getScope(node: ASTNode): mutable.HashMap[String, ArrayBuffer[ITypeBinding]] = {
    val scope: mutable.HashMap[String, ArrayBuffer[ITypeBinding]] = new  mutable.HashMap[String, ArrayBuffer[ITypeBinding]]
    if (node != null) {
      val visitor: ScopeCollector = new ScopeCollector(scope)
      node.accept(visitor)
    }
    return scope
  }

  def getFieldAccess(node: ASTNode): mutable.HashMap[String, ArrayBuffer[ITypeBinding]] = {
    val fieldAcc: mutable.HashMap[String, ArrayBuffer[ITypeBinding]] = new  mutable.HashMap[String, ArrayBuffer[ITypeBinding]]
    if(node != null){
      val visitor = new FieldAccessCollector(fieldAcc)
      node.accept(visitor)
    }
    return fieldAcc
  }

  def getVariableUsed(node: ASTNode): mutable.HashMap[String, ArrayBuffer[ITypeBinding]] = {
    var variablesUsed: mutable.HashMap[String, ArrayBuffer[ITypeBinding]] = new mutable.HashMap[String, ArrayBuffer[ITypeBinding]]
    variablesUsed=getScope(node)
    getFieldAccess(node).map{
      case (name, associatedTypes) =>{
        variablesUsed.get(name) match {
          case None => {
            val types = new ArrayBuffer[ITypeBinding]()
            types.appendAll(associatedTypes)
            variablesUsed.put(name,types)
          }
          case Some(v) => v.appendAll(associatedTypes)
        }
      }
    }
    return variablesUsed
  }

  def createNodeFromString(toRep: String): ASTNode = {
    // Note that we need to create stub code for JDT to parse appropriately and then get back
    // the JDT ASTNode we want to create from the return statement
    val document = new Document("public class X{ public void replace(){return "+toRep+";}}")
    val parser = ASTParser.newParser(8)
    parser.setSource(document.get().toCharArray())
    val cu = parser.createAST(null).asInstanceOf[CompilationUnit]
    val visitor = new ASTVisitor() {
      var toRepNode : ASTNode = null
      override def visit(node: ReturnStatement): Boolean = {
        toRepNode = node.getExpression
        return false
      }
    }
    cu.accept(visitor)
    //println("To Rep Node: "+visitor.toRepNode)
    return visitor.toRepNode
  }

  def main(args: Array[String]) : Unit = {
    createNodeFromString("(a + b - c) * d < e")
  }
}
