package utils

import localization.Identifier
import org.eclipse.jdt.core.dom._
import org.apache.log4j.Logger

import scala.collection.mutable.ArrayBuffer

/**
 * Created by dxble on 8/22/15.
 */

// Note: in this case currentBuggyNode must be of type method invocation or expression statement of method invocation
// Do not collect methods that might make changes to global variables

class MethodCallInforCollector(currentBuggyNode: Identifier[Any]) extends ASTVisitor{
  private val logger = Logger.getLogger(classOf[MethodCallInforCollector])

  val possibleInvokers = new ArrayBuffer[Expression]()
  val possibleMethodCallRep = new ArrayBuffer[IMethodBinding]()
  var buggyNodeInvokerType: ITypeBinding = null
  var buggyNodeMethodName: SimpleName = null
  var buggyNodeMethodCallRetType: ITypeBinding = null
  var buggyNodeTypeBinding: IMethodBinding = null
  var mc: MethodInvocation = null

  private def resolveTypeOfInvoker(): MethodInvocation ={
    if(mc == null) {
      if (currentBuggyNode.transformToJavaNode()) {
        val jvNode = currentBuggyNode.getJavaNode()
        //println(ASTNode.nodeClassForType(jvNode.getNodeType))
        if(jvNode.isInstanceOf[ExpressionStatement]) {
          val exp = jvNode.asInstanceOf[ExpressionStatement].getExpression
          if(exp.isInstanceOf[MethodInvocation])
            mc = exp.asInstanceOf[MethodInvocation]
        }
        if (jvNode.isInstanceOf[MethodInvocation]) {
          mc = jvNode.asInstanceOf[MethodInvocation]
        }
        if(mc.getExpression() != null)
         buggyNodeInvokerType = mc.getExpression.resolveTypeBinding()
        if(mc.getName() != null) {
          buggyNodeMethodName = mc.getName
          buggyNodeMethodCallRetType=buggyNodeMethodName.resolveTypeBinding()
        }
        this.buggyNodeTypeBinding = mc.resolveMethodBinding()
        return mc
        //println("Resolved: "+buggyNodeInvokerType)
      }
    }
    return mc
    //throw new RuntimeException("Not a valid method invocation mutation!")
  }

  //TODO: check this implementation
  override def visit (node : FieldDeclaration) : Boolean = {
    import scala.jdk.CollectionConverters._
    val mc = resolveTypeOfInvoker()
    for (o <- node.fragments.asScala) {
      if (o.isInstanceOf[VariableDeclarationFragment]) {
        val v: VariableDeclarationFragment = o.asInstanceOf[VariableDeclarationFragment]
        if(compatibleTypes(v.resolveBinding().getType, buggyNodeInvokerType) && !v.getName.getIdentifier.equals(mc.getExpression.toString))
          this.possibleInvokers.append(v.getName)
      }
    }
    return super.visit(node)
  }

  override def visit (node : MethodDeclaration) : Boolean = {
    val nodeLine=ASTUtils.getStatementLineNo(node)
    val nodeEndline=ASTUtils.getStatementEndLineNo(node)
    resolveTypeOfInvoker()
    val nodeBinding = node.resolveBinding()
    val nodeRetType=nodeBinding.getReturnType
    // Constrain that the current method is not the one that contains the buggy exp
    // TODO: do not know why this condition is helpful yet.
    if(!(nodeLine < currentBuggyNode.getBeginLine().toInt && nodeEndline >= currentBuggyNode.getEndLine().toInt)) {
      if (compatibleTypes(nodeRetType, buggyNodeInvokerType)) {
        val mc = currentBuggyNode.getJavaNode().getAST.newMethodInvocation()
        mc.setName(currentBuggyNode.getJavaNode().getAST.newSimpleName(node.getName.getIdentifier))
        possibleInvokers.append(mc)
      }

      if(compatibleTypes(buggyNodeMethodCallRetType, nodeRetType)) {
        /*if(node.parameters().size() == mc.arguments().size()){
          // check same args
          logger.debug(buggyNodeTypeBinding.getParameterTypes.size)
          buggyNodeTypeBinding.getParameterTypes.map(t => logger.debug(t.toString))
          nodeBinding.getParameterTypes.map(t => logger.debug(t.toString))
          if((buggyNodeTypeBinding.getParameterTypes,nodeBinding.getParameterTypes).zipped.exists{
            (nodeArg, buggyNodeArg) =>
              !ASTUtils.strictlyCompatibleTypes(nodeArg, buggyNodeArg)
          })
          {

          } else
            possibleMethodCallRep.append(node.getName)
        }*/
        // A possible replacement if it has the same method return type as the buggy one
        // Also, all its parameterTypes should be repairable ones, e.g., int, boolean, etc
        if(buggyNodeTypeBinding.getParameterTypes.length ==0
          /*|| !buggyNodeTypeBinding.getParameterTypes.exists(t => !Repairable.canHandleType(t))*/)
          if(nodeBinding.toString.compareTo(mc.resolveMethodBinding().toString) != 0)
            possibleMethodCallRep.append(nodeBinding)
      }

    }
    return true
  }

  private def compatibleTypes(t1: ITypeBinding, t2: ITypeBinding): Boolean ={
    if(t1 == null || t2 == null)
      return false

    if (t1.isSubTypeCompatible(t2) || t1.isCastCompatible(t2)
      || t1.isAssignmentCompatible(t2) || t1.toString.equals(t2.toString)) {
      return true
    }else
      return false
  }
}

