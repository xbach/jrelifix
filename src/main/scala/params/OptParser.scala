package params

import localization.{LocalizationLibName}
import localization.LocalizationLibName.LocalizationLibName


case class Config(
                   classPath: String = "",

                   sourceFolder: String = "",
                   testFolder: String = "",
                   sourceClassFolder: String = "",
                   testClassFolder: String = "",

                   projFolder: String = "",

                   onlyFailTests: Boolean = false,
                   passingTests: Seq[String] = null,
                   failingTests: Seq[String] = null,
                   goldenFailingTests: Seq[String] = null,
                   goldenTestFolder: String = null,

                   testRunner: String = "junit", //junit or testng
                   testTimeout: Int = 100,
                   testMethodNamePatterns: Seq[String] = null,
                   testsIgnored: Seq[String] = null,
                   testsFullNameIgnored: Seq[String] = null,
                   collectObjectVar: Boolean = false,

                   locLibName: LocalizationLibName = LocalizationLibName.JAGUAR,
                   isDataFlow: Boolean = false,
                   locHeuristic: String = "Ochiai",
                   faultLines: String = null,
                   topNFaultLoc: Int = 100,

                   javaHome: String = null,
                   ignorePositiveTestsFail: Boolean = false,
                   verbose: Boolean = false,
                   debug: Boolean = false)

object OptParser {
  val builder = new scopt.OptionParser[Config]("scopt")
  {

      head("scopt", "4.x")

      opt[Unit]("verbose")
        .action((_, c) => c.copy(verbose = true))
        .text("verbose is a flag")

      opt[Unit]("debug")
        .hidden()
        .action((_, c) => c.copy(debug = true))
        .text("this option is hidden in the usage text")

      help("help").text("prints this usage text")

      opt[String]( "classpath")
        .action((cp, c) => c.copy(classPath = cp))
        .text("Classpath")

      opt[String]( "javaHome")
        .action((cp, c) => c.copy(javaHome = cp))
        .text("Java Home Folder")

      opt[String]( "sourceFolder")
        .action((source, c) => c.copy(sourceFolder = source))
        .text("Folder of source code, e.g., blah/src")

      opt[String]( "sourceClassFolder")
        .action((source, c) => c.copy(sourceClassFolder = source))
        .text("Folder of classes of compiled source code, e.g., blah/target/classes")

      opt[Boolean]("onlyFailTests")
        .action((o, c) => c.copy(onlyFailTests = o))
        .text("Run only fail tests")

      opt[Boolean]("igorePositiveTestsFail")
        .action((o, c) => c.copy(ignorePositiveTestsFail = o))
        .text("Ignore positive tests that fail when dry run")

      opt[Seq[String]]("passingTests")
        .action((p, c) => c.copy(passingTests = p))

      opt[Seq[String]]("failingTests")
        .action((p, c) => c.copy(failingTests = p))

      opt[Seq[String]]("goldenfailingTests")
        .action((p, c) => c.copy(goldenFailingTests = p))

      opt[String]( "testFolder")
        .action((t, c) => c.copy(testFolder = t))
        .text("Folder of tests, e.g., blah/tests")

      opt[String]( "testClassFolder")
        .action((t, c) => c.copy(testClassFolder = t))
        .text("Folder of classes of tests, e.g., blah/target/test-classes")

      opt[String]( "projectFolder")
        .action((t, c) => c.copy(projFolder = t))
        .text("Folder of project, e.g., blah")

      opt[String]( "goldenTestFolder")
        .action((t, c) => c.copy(goldenTestFolder = t))
        .text("Folder of golden tests, e.g., blah/tests")

      opt[String]( "testRunner")
        .action((t, c) => c.copy(testRunner = t))
        .text("Run tests using junit or testng")

      opt[Int]("testTimeout")
        .action((t, c) => c.copy(testTimeout = t))
        .text("Timeout for running tests, in seconds")

      opt[Seq[String]]("testMethodNamePatterns")
        .action((p, c) => c.copy(testMethodNamePatterns = p))

      opt[Seq[String]]("testsIgnored")
        .action((p, c) => c.copy(testsIgnored = p))

      opt[Seq[String]]("testsFullNameIgnored")
        .action((p, c) => c.copy(testsFullNameIgnored = p))

      opt[Boolean]("collectObjectVariables")
        .action((o, c) => c.copy(collectObjectVar = o))
        .text("To collect object variables in visible variables collector")

      opt[LocalizationLibName]("locLib")
        .action((o, c) => c.copy(locLibName = o))
        .text("Name of localization library: jaguar or gzoltar")

      opt[String]("locHeuristic")
        .action((o, c) => c.copy(locHeuristic = o))
        .text("Name of heuristic for fault localization, e.g., Ochiai, Tarantula, etc")


      opt[String]("faultLines")
        .action((o, c) => c.copy(faultLines = o))
        .text("Faulty lines with class names, e.g., a.b.c.XYZ: ")

      opt[Int]("topNFaultLoc")
        .action((o, c) => c.copy(topNFaultLoc = o))
        .text("Top N Fault Locations to be considered, default is 100")

      opt[Boolean]("isDataFlow")
        .action((o, c) => c.copy(isDataFlow = o))
        .text("Option for Jaguar fault localization tool")

  }

  implicit val localizationLibNameRead: scopt.Read[LocalizationLibName.Value] =
    scopt.Read.reads(LocalizationLibName withName _)

  private var params: Config = null

  def parseOpts(args: Array[String]): Unit = {
    // OParser.parse returns Option[Config]
    params = builder.parse(args, Config()) match {
      case Some(config) => config
      // do something
      case _ => throw new RuntimeException("Parsing arguments error!")
      // arguments are bad, error message will have been displayed
    }
  }

  def parsedParams(): Config = params

  def main(args: Array[String]): Unit = {
    parseOpts(Array())
  }
}
