package testrunner

import java.io.File
import java.net.{MalformedURLException, URL}

import driver.Main
import junithandler.{JUnitExecutorProcess, TestResult}
import org.apache.log4j.Logger
import params.OptParser

import scala.collection.mutable.ArrayBuffer

/**
 * Created by xuanbach32bit on 8/27/15.
 */

class JUnitTestInvoker() extends AbstractTestInvoker{

  var negTests: ArrayBuffer[NegativeTest] = null
  var posTests: ArrayBuffer[PositiveTest] = null

  def init(): Unit ={
    negTests = negativeTestName()
    posTests = positiveTestsName(OptParser.parsedParams().testFolder, negTests)
  }

  init()

  // return true if all tests pass. Otherwise, return false and list of failing tests
  def invokeAllTests(withTracing: Boolean, withDumping: Boolean, configValidation: Boolean, classPaths: Array[URL]) : (Boolean, Array[TestCase[Any]]) = {
    // Note that we do not dump when running negative tests.
    // We already dump expected output for negative tests using assert file.
    //val totalTests = negTests.size + posTests.size
    val (negPassAll,negFail) = invokeTests(withTracing, false, configValidation, negTests, classPaths)
    val (posPassAll,posFail) = invokeTests(withTracing, withDumping, configValidation, posTests, classPaths)
    return (negPassAll && posPassAll, negFail ++ posFail)
  }

  def invokeTests(withTracing: Boolean, withDumping: Boolean, configValidation: Boolean,
                  tests: ArrayBuffer[_<:TestCase[Any]], classPaths: Array[URL]) : (Boolean, Array[TestCase[Any]]) = {
    var allTestsPass = true
    val failedTests = tests.foldLeft(Array[TestCase[Any]]())((res, t) => {
      val (pass, _) = invokeTest(t, withTracing, withDumping, classPaths)
      if(configValidation)
        if(pass && t.isInstanceOf[NegativeTest])
          throw new RuntimeException("Negative Test pass: " + t + " ==> check your failingTests configuration!")
        else if(!pass && t.isInstanceOf[PositiveTest])
          throw new RuntimeException("Positive Test fail: " + t + " ==> check your failingTests configuration!")

      if (!pass) {
        allTestsPass = false
        res :+ t
      } else {
          res
      }
    })
    (allTestsPass, failedTests)
  }

  def findTestWithName(testName: String): TestCase[Any] = {
    negTests.find(n => n.getFullNameWithMethod().compareTo(testName) == 0).getOrElse(posTests.find(p => p.getFullNameWithMethod().compareTo(testName) ==0).getOrElse(null))
  }

  def invokeOneTestFresh(test: TestCase[Any], withTracing: Boolean, withDumping: Boolean,
                         classPaths: Array[URL]): (Boolean, Boolean) ={
    val temp = invokeTest(test, withTracing, withDumping, classPaths)
    temp
  }

  def invokeTestsFresh(withTracing: Boolean, withDumping: Boolean, configValidation: Boolean,
                  tests: ArrayBuffer[_<:TestCase[Any]], classPaths: Array[URL]) : (Boolean, Array[TestCase[Any]]) = {
    val temp = invokeTests(withTracing, withDumping, configValidation, tests, classPaths)
    temp
  }

  override def invokeTest(test: TestCase[Any], withTracing: Boolean, withDumping: Boolean, classPaths: Array[URL]): (Boolean, Boolean) ={
    var testRes : TestResult = null
    testRes = validate(test, Array(), classPaths)
    if(testRes != null)
    {
      //println(testRes)
      (testRes.wasSuccessful(), false) // compilation error false because a variant gone through validation step is a compilable one already
    }else
      (false,false)
  }

  private def validate (test: TestCase[Any], properties: Array[String], classPaths: Array[URL]): TestResult = {
    try {
      val p: JUnitExecutorProcess = new JUnitExecutorProcess
      /*classOf[SingleJUnitTestRunner].getCanonicalName*/
      logger.debug("Run test: "+test)
      properties.foreach(p=>
        logger.debug("Props: "+p)
      )
      val runner = if(OptParser.parsedParams().testRunner.compareTo("junit") == 0) "junithandler.SingleJUnitTestRunner" else if(OptParser.parsedParams().testRunner.compareTo("testng") == 0) "junithandler.SingleTestNGTestRunner" else throw new RuntimeException("Not rupported test runer type!")
      val trfailing: TestResult = p.execute(classPaths,test.getFullNameWithMethod(), runner, OptParser.parsedParams().testTimeout, properties)
      return trfailing
    }
    catch {
      case e: MalformedURLException => {
        e.printStackTrace
        throw e
      }
    }
  }

}
