package localization


import br.usp.each.saeg.jaguar.core.model.core.requirement.LineTestRequirement
import br.usp.each.saeg.jaguar.core.runner.JaguarRunListener
import br.usp.each.saeg.jaguar.core.utils.FileUtils
import br.usp.each.saeg.jaguar.core.{JaCoCoClient, Jaguar}
import org.apache.log4j.Logger
import org.junit.runner.JUnitCore

import scala.collection.mutable.ArrayBuffer

abstract sealed class LocalizationLibrary() {
  val rankedList: ArrayBuffer[Identifier[Int]] = new ArrayBuffer[Identifier[Int]]()

  def runner(): Unit
}
case class PredefinedFaults(faultLines: String) extends LocalizationLibrary(){
  // faultLines's format: x.y.z.ABC:1 2 7 8,3 5 7 9;m.n.p.KHG:3 4 6 5

  override def runner(): Unit = faultLines.split(";").foreach{
    case l => {
      val sp = l.trim.split(":") // sp(0) is file name, sp(1) is faulty lines
      sp(1).split(",").foreach{
        case fl => {
          val iden = new FaultIdentifier(fl.trim.split(" "), sp(0))
          rankedList.append(iden)
        }
      }
    }
  }
}
case class JaguarLocalization(config: JaguarConfig) extends LocalizationLibrary(){
  val logger = Logger.getLogger(this.getClass)

  private lazy val junit = new JUnitCore

  override def runner(): Unit = {
    val classes = FileUtils.findTestClasses(config.testDir)
    val jaguar = new Jaguar(config.sourceDir)
    val client = new JaCoCoClient(config.isDataFlow)
    client.connect()
    this.junit.addListener(new JaguarRunListener(jaguar, client))
    this.junit.run(classes:_*) // Super note: adding the _* tells the compiler to treat array as varargs in Java
    client.close()
    //if (this.outputType == "H") jaguar.generateHierarchicalXML(this.heuristic, this.projectDir, this.outputFile)
    //else jaguar.generateFlatXML(this.heuristic, this.projectDir, this.outputFile)
    val rankedList = jaguar.generateRank(config.heuristic)
    logger.info("Finished localization.....!")
    rankedList.forEach(e => {
      val ej = new JaguarFaultIdentifier(e.getClassName, e.asInstanceOf[LineTestRequirement].getLineNumber, e.getSuspiciousness)
      this.rankedList.append(ej)
    })
  }
}