package localization

import java.io.File

import br.usp.each.saeg.jaguar.core.heuristic.Heuristic

object LocalizationLibName extends Enumeration {
  type LocalizationLibName = Value
  val  JAGUAR, GZOLTAR = Value
}

abstract sealed class LocalizationConfig {
}
case class JaguarConfig (heuristic: Heuristic, projectDir: File, sourceDir: File, testDir: File, isDataFlow: Boolean) extends LocalizationConfig {
}
case class GZoltarConfig() extends LocalizationConfig {
}
