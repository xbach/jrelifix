package parser.javaparser

/**
  * Created by dxble on 8/27/16.
  */
import java.io.File

import driver.Main
import utils.FileFolderUtils
import parser.AbstractParser
import org.eclipse.jdt.core.JavaCore
import org.eclipse.jdt.core.dom._
import params.OptParser
import testrunner.TestCase

import scala.collection.mutable

object JavaParser extends AbstractParser[CompilationUnit]{
  def parseOneFile(filePath: String, sourceFolder: String, unitName: String): (CompilationUnit, String) = {
    val content = readFileToString(filePath, null, sourceFolder, false)
    return (parse(content, filePath, sourceFolder, unitName), content)
  }

  //Note when run in intellij, check project structure => modules => dependencies => move eclipse jdt core before the br.... because that br package use another conflicting version of jdt
  def parse[B >:CompilationUnit](str: String, filePath: String, sourceFolder: String, unitName: String): CompilationUnit = {
    val parser: ASTParser = ASTParser.newParser(8)
    //parser.setEnvironment(RepairOptions.libs,Array(file.getParent, file.getParentFile.getParent), null, true)
    // See Note ABOVE
    parser.setEnvironment(OptParser.parsedParams().classPath.split(":"), Array(new File(sourceFolder).getParent), null, true)
    parser.setUnitName(unitName)
    //parser.setEnvironment(Array(str), Array(str),Array("UTF-8"), true)
    parser.setSource(str.toCharArray)
    parser.setKind(ASTParser.K_COMPILATION_UNIT)
    parser.setResolveBindings(true)
    parser.setBindingsRecovery(true)
    parser.setStatementsRecovery(true)

    val options = JavaCore.getOptions()
    JavaCore.setComplianceOptions(JavaCore.VERSION_1_5, options);
    parser.setCompilerOptions(options)

    try {
      val root = parser.createAST(null)
      val cu: CompilationUnit = root.asInstanceOf[CompilationUnit]
      cu.recordModifications
      assert(cu != null)
      return cu
    }catch {
      case e: Throwable => {
        println(filePath)
        e.printStackTrace()
        sys.error("CU is null when parsing!!!"+filePath)
      }
    }
  }

  def getCompilationUnit(fileName: String): CompilationUnit ={
    globalASTs.getOrElse(fileName, null)
  }

  def getPackageName[B >:CompilationUnit](cu: CompilationUnit, filePath: String): String = {
    val pkg=cu.getPackage
    val file = new File(filePath)
    if(pkg!=null) {
      //val pkg_name=pkg.toString.stripLineEnd.split(" ")(1).split(";")(0)
      val pkg_name=pkg.getName.getFullyQualifiedName
      return pkg_name + "." + file.getName.split("\\.")(0)
    }else{
      return file.getName.split("\\.")(0) // this is to strip the .java at the end
    }
  }

  override def batchParse(dirPath: String, classPath: String, faultFiles: Set[String]): Unit = {
    val files: java.util.List[File] = FileFolderUtils.walk(dirPath,".java", new java.util.ArrayList[File])

    import scala.jdk.CollectionConverters._
    val sourceFilePaths = files.asScala.collect {
      case f: File if f.isFile && TestCase.isNotTestFile(f) => f.getAbsolutePath
    }

    val astParser = ASTParser.newParser(8)

    // set up libraries (.jar, .class or .java)
    // Array(new File(Options.sourceFolder).getParent)
    // new File(Main.options.sourceFolder).toString
    astParser.setEnvironment(classPath.split(":"), Array(dirPath), null, true)

    astParser.setResolveBindings(true)

    // with Bingding Recovery on, the compiler can detect
    // binding among the set of compilation units
    astParser.setBindingsRecovery(true)

    // set default options, especially for Java 1.5
    val options = JavaCore.getOptions()
    JavaCore.setComplianceOptions(JavaCore.VERSION_1_5, options)
    astParser.setCompilerOptions(options)


    val requestor = new FileASTRequestor() {
      override def acceptAST(sourceFilePath: String, ast: CompilationUnit) : Unit = {
        val fileName = convertFilePath2FilePackageName(dirPath, sourceFilePath)
        // if file being read is a fault file, keep its content
        // println("Processed FileName"+fileName)
        if(faultFiles.contains(fileName)) {
          readFileToString(sourceFilePath, faultFiles, dirPath)
        }
        globalASTs.put(getPackageName(ast,sourceFilePath), ast)
      }

      override def acceptBinding(bindingKey: String, binding: IBinding) : Unit = {
        // do nothing
        // System.out.println("Accept Binding:... " + bindingKey);
        // System.out.println(binding);
      }
    }

    astParser.createASTs(sourceFilePaths.toArray,
      null, 			/*  use default encoding */
      Array[String](), /* no binding key */
      requestor,
      null			/* no IProgressMonitor */
    )
  }

}
