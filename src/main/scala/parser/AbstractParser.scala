package parser

/**
  * Created by dxble on 8/27/16.
  */

import java.io.{BufferedReader, File, FileReader, IOException}
import java.util

import driver.Main
import utils.FileFolderUtils

import testrunner.TestCase

import scala.collection.mutable

/**
  * Created by xuanbach32bit on 5/17/15.
  */
abstract class AbstractParser[T] {
  //keep ASTs of all files being parsed
  val globalASTs: scala.collection.mutable.HashMap[String, T] = new scala.collection.mutable.HashMap[String, T]

  //keep original contents of files being edited for repair
  val modifyingFiles: scala.collection.mutable.HashMap[String, String] = new scala.collection.mutable.HashMap[String,String]

  def parse[B >: T](fileContent:String, filePath: String, sourceFolder: String, unitName: String): T
  def batchParse(dirPath: String, classPath: String, faultFiles: Set[String]): Unit

  def getPackageName[B >: T](unitContainingFile: T, path: String): String

  // this is to avoid keeping all files' contents
  protected def addModifyingFiles(fileName: String, content: String) = {
      modifyingFiles.put(fileName, content)
  }

  protected def convertFilePath2FilePackageName(base: String, filePath: String): String ={
    val fileRelativePath=FileFolderUtils.relativePath(base, filePath) //
    val fileName = FileFolderUtils.path2Package(fileRelativePath)
    return fileName
  }

  @throws(classOf[IOException])
  protected def readFileToString(filePath: String, faultFiles: Set[String], sourceFolder: String, add2Modif: Boolean = true): String = {
    val fileData: StringBuilder = new StringBuilder(1000)
    val reader: BufferedReader = new BufferedReader(new FileReader(filePath))
    var buf: Array[Char] = new Array[Char](10)
    var numRead: Int = 0
    while ((({
      numRead = reader.read(buf); numRead
    })) != -1) {
      //System.out.println(numRead)
      val readData: String = String.valueOf(buf, 0, numRead)
      fileData.append(readData)
      buf = new Array[Char](1024)
    }
    reader.close()
    // if file being read is a fault file, keep its content
    // println("Processed FileName"+fileName)
    if(add2Modif) {
      val fileName = convertFilePath2FilePackageName(sourceFolder, filePath)
      if(faultFiles.contains(fileName))
        addModifyingFiles(fileName, fileData.toString())
    }

    return fileData.toString
  }
}
